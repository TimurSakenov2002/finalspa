package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatService {

    @Autowired
    private ChatRepository _chatRepository;

    public Optional<Chat> findById(Long id) {
        return _chatRepository.findById(id);
    }

    public Chat save(Chat chat) {
        return _chatRepository.save(chat);
    }

    public void deleteById(Long id) {
        _chatRepository.deleteById(id);
    }

}

