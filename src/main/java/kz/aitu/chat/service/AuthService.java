package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {

    @Autowired
    private AuthRepository authRepository;

    public String login(String login, String password) {
        Auth auth = authRepository.findAuthByLoginAndPassword(login, password);
        if(auth != null) {
            auth.setToken(UUID.randomUUID().toString());
            authRepository.save(auth);
            return "Successfully";
        } else {
            return "Error";
        }
    }

    public String register(Auth auth) {
        if (authRepository.findAuthByRegister(auth.getLogin()) != null && authRepository.findAuthByRegister(auth.getPassword()) != null) {
            auth.setToken(UUID.randomUUID().toString());
            authRepository.save(auth);
            return "SuccessfullyRegistered";
        } else {
            return "Bad!";
        }
    }

    public String findToken(String token) {
        Auth auth = authRepository.findAuthByToken(token);
        if(auth != null) {
            return "SuccessToken";
        } else {
            return "Error";
        }
    }
}
