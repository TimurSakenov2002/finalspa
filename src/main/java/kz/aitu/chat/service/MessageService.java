package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public Optional<Message> findById(Long id) {
        return messageRepository.findById(id);
    }

    public Message addMessage(Message message) throws Exception {
        if (message == null) throw new Exception("This message is empty!");
        if (message.getId() != null) throw new Exception("This message has id");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("This message is blank");
        return messageRepository.save(message);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public Message update(Message updatedMessage) throws Exception {
        if (updatedMessage == null) throw new Exception("This message is empty!");
        if (updatedMessage.getId() == null) throw new Exception("This message id is empty!");
        if (updatedMessage.getText() == null || updatedMessage.getText().isBlank()) throw new Exception("This message's text is either empty or just blank!");

        Optional<Message> messageDb = messageRepository.findById(updatedMessage.getId());
        if (messageDb.isEmpty()) throw new Exception("This message wasn't found");
        updatedMessage.setText(updatedMessage.getText());
        Message message = messageDb.get();
        message.setText(updatedMessage.getText());
        return save(message);
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }
}

