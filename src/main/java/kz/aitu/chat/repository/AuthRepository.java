package kz.aitu.chat.repository;

import kz.aitu.chat.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {

    Auth findAuthByLoginAndPassword(String login, String password);

    Auth findAuthByToken(String token);

    Auth findAuthByRegister(String login);
}
