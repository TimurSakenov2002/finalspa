package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private AuthService authService;

    @PostMapping("/edit/{message_id}")
    public ResponseEntity<?> editMessage(@RequestBody Message message, @RequestHeader String token) throws Exception {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        messageService.update(message);
        return ResponseEntity.ok("Message was edited!");

    }

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message, @RequestHeader String token) throws Exception {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        messageService.addMessage(message);
        return ResponseEntity.ok("Message was added!");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long id, @RequestHeader String token) {
        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        messageService.deleteById(id);
        return ResponseEntity.ok("Message was deleted!");

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader String token) {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        messageService.findById(id);
        return ResponseEntity.ok(messageService.findById(id));
    }
}

