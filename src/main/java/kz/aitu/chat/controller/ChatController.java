package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private AuthService authService;

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader String token) {

        if(authService.findToken(token).equals("SuccessToken")){
            return ResponseEntity.ok(chatService.findById(id));
        }

        return ResponseEntity.ok("Invalid token!");

    }

    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader String token) {

        if(!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token!");
        }
        chatService.save(chat);
        return ResponseEntity.ok("Chat has been added!");

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id, @RequestHeader String token) {

        if(authService.findToken(token).equals("SuccessToken")){
            try {
                chatService.deleteById(id);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
            return ResponseEntity.ok("Chat has been deleted");
        }

        return ResponseEntity.ok("Invalid token!");
    }
}

