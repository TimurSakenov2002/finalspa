package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/login")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login (@RequestParam String login, @RequestParam String password) {
        String response = authService.login(login, password);
        if (response.equals("Successfully")) {
            return ResponseEntity.ok("You are logged-in!");
        } else {
            return ResponseEntity.ok("Invalid login or password!");
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> register (@RequestBody Auth auth) {
        String response = authService.register(auth);
        if(response.equals("SuccessfullyRegistered"))
            return ResponseEntity.ok("You've been successfully registered!");
        else
            return ResponseEntity.ok("There's something error!");
    }

}
