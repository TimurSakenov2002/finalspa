package kz.aitu.chat.controller;

import kz.aitu.chat.model.Users;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/users")
@AllArgsConstructor
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private AuthService authService;

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader String token) {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        usersService.findById(id);
        return ResponseEntity.ok(usersService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addUser(@RequestBody Users user, @RequestHeader String token) {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        usersService.save(user);
        return ResponseEntity.ok("User was added!");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id, @RequestHeader String token) {

        if (!authService.findToken(token).equals("SuccessToken")) {
            return ResponseEntity.ok("Invalid token");
        }

        usersService.deleteById(id);
        return ResponseEntity.ok("User was deleted");
    }
}
